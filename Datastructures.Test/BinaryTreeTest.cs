﻿using Datastructures.CL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Datastructures.Test
{
    [TestClass]
    public class BinaryTreeTest
    {
        BinaryTree<int> sut;
        Random rand = new Random();

        [TestInitialize]
        public void Init()
        {
            sut = new BinaryTree<int>();
        }

        [TestMethod]
        public void AddToTreeTest()
        {
            sut.Insert(3);
            sut.Insert(2);
            sut.Insert(4);

            Assert.IsTrue(sut.Contains(3));
            Assert.IsTrue(sut.Contains(2));
            Assert.IsTrue(sut.Contains(4));
            Assert.AreEqual(sut.Count, 3);
        }

        [TestMethod]
        public void InOrderTraversalTest()
        {
            sut.Insert(3);
            sut.Insert(2);
            sut.Insert(4);
            sut.Insert(54);
            sut.Insert(87);
            sut.Insert(34);

            List<int> actual = new List<int>();

            foreach(var value in sut)
            {
                actual.Add(value);
            }

            CollectionAssert.AreEqual(actual, new List<int> { 2, 3, 4, 34, 54, 87});
        }

        [TestMethod]
        [CsvTestSource("CCTestData.csv")]
        public void RemoveElementTest(int[] nums)
        {
            sut.InsertRange(nums);

            int toRemove = nums[rand.Next(nums.Length - 1)];
            Logger.LogMessage("Value being removed: {0}", toRemove);
            List<int> expected = nums.OrderBy(x => x).ToList();
            expected.Remove(toRemove);
            sut.Remove(toRemove);

            List<int> actual = new List<int>();

            foreach (var value in sut)
            {
                actual.Add(value);
            }

            CollectionAssert.AreEqual(actual, expected);
        }
    }
}
