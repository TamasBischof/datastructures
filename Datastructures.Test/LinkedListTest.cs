using Datastructures.CL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Datastructures.Test
{
    [TestClass]
    public class LinkedListTest
    {
        //the list used for testing
        LinkedList<int> list;

        [TestInitialize]
        public void InitTest()
        {
            list = new LinkedList<int>();
        }

        [TestMethod]
        [DataTestMethod]
        [CsvTestSource("CCTestData.csv")]
        public void ListClearTest(int[] cases)
        {
            list.AddRange(cases);

            list.Clear();

            Assert.IsTrue(list.Empty);
            Assert.AreEqual(list.Count, 0);
        }

        [TestMethod]
        [DataTestMethod]
        [CsvTestSource("CCTestData.csv")]
        public void ListContainsTest(int[] cases)
        {
            list.AddRange(cases);

            foreach (int num in cases)
            {
                Assert.IsTrue(list.Contains(num));
            }
            Assert.AreEqual(list.Count, cases.Count());
        }

        [TestMethod]
        [DataTestMethod]
        [CsvTestSource("CCTestData.csv")]
        public void ListAddToTailTest(int[] cases)
        {
            list.AddRange(cases);
            int tailValue = cases[1];

            list.AddToTail(tailValue);

            Assert.AreEqual(list.Tail.Value, tailValue);
            Assert.AreEqual(list.Head.Value, cases[0]);
            Assert.AreEqual(list.Count, cases.Count() + 1);
            Assert.IsFalse(list.Empty);
        }
    }
}
