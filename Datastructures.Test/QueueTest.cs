﻿using Datastructures.CL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;

namespace Datastructures.Test
{
    [TestClass]
    public class QueueTest
    {
        //queue under test
        Queue<int> q;

        [TestInitialize]
        public void TestInit()
        {
            q = new Queue<int>();
        }

        [TestMethod]
        public void QueueAddTest()
        {
            q.Enqueue(1);
            q.Enqueue(2);

            Assert.AreEqual(q.Count, 2);
            Assert.AreEqual(q.Peek(), 1);
        }

        [TestMethod]
        public void QueueRemoveTest()
        {
            q.Enqueue(1);
            q.Enqueue(2);

            Assert.AreEqual(q.Dequeue(), 1);
            Assert.AreEqual(q.Count, 1);
            Assert.AreEqual(q.Peek(), 2);
        }

        [TestMethod]
        public void QueueClearTest()
        {
            q.Enqueue(1);
            q.Enqueue(2);

            q.Clear();

            Assert.AreEqual(q.Count, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(CollectionEmptyException))]
        public void QueueRemoveWhenEmptyTest()
        {
            q.Enqueue(1);
            q.Dequeue();
            q.Dequeue();
        }

        [TestMethod]
        [ExpectedException(typeof(CollectionEmptyException))]
        public void QueuePeekWhenEmptyTest()
        {
            q.Enqueue(1);
            q.Dequeue();
            q.Peek();
        }
    }
}
