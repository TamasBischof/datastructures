﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Datastructures.Test
{
    public class CsvTestSource : Attribute, ITestDataSource
    {
        string fileName;

        public CsvTestSource(string fileName) => this.fileName = fileName;

        public IEnumerable<object[]> GetData(MethodInfo methodInfo)
        {
            string[] lines = File.ReadAllLines(fileName);

            List<object[]> result = new List<object[]>();

            foreach (var line in lines)
            {
                int[] values = line.Split(',').Select(x => int.Parse(x)).ToArray();

                result.Add(new object[] { values });
            }

            return result;
        }

        public string GetDisplayName(MethodInfo methodInfo, object[] data)
        {
            return string.Format("{0} ({1})", methodInfo.Name, string.Join(',', data[0] as int[]));
        }
    }
}
