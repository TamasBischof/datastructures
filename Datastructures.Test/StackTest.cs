﻿using Datastructures.CL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;

namespace Datastructures.Test
{
    [TestClass]
    public class StackTest
    {
        //stack under test
        Stack<int> stack;

        [TestInitialize]
        public void TestInit()
        {
            stack = new Stack<int>();
        }

        [TestMethod]
        public void StackClearTest()
        {
            stack.Push(1);
            stack.Push(1);
            stack.Push(1);
            stack.Clear();
            
            Assert.AreEqual(stack.Count, 0);
        }

        [TestMethod]
        public void StackPushTest()
        {
            stack.Push(1);

            Assert.AreEqual(stack.Peek(), 1);
            Assert.AreEqual(stack.Count, 1);
        }

        [TestMethod]
        public void StackPopTest()
        {
            stack.Push(2);
            stack.Push(1);

            Assert.AreEqual(stack.Pop(), 1);
            Assert.AreEqual(stack.Count, 1);
            Assert.AreEqual(stack.Peek(), 2);
        }

        [TestMethod]
        [ExpectedException(typeof(CollectionEmptyException))]
        public void StackRemoveWhenEmptyTest()
        {
            stack.Push(3);
            stack.Push(1);

            stack.Pop();
            stack.Pop();
            stack.Pop();
        }

        [TestMethod]
        [ExpectedException(typeof(CollectionEmptyException))]
        public void StackPeekWhenEmptyTest()
        {
            stack.Push(3);
            stack.Push(1);

            stack.Pop();
            stack.Pop();
            stack.Peek();
        }
    }
}
