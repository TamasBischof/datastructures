﻿using Datastructures.CL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Datastructures.Test
{
    [TestClass]
    public class DynamicStackTest
    {
        DynamicStack<int> stack;

        [TestInitialize]
        public void Init()
        {
            stack = new DynamicStack<int>();
        }

        [TestMethod]
        [DataTestMethod]
        [CsvTestSource("CCTestData.csv")]
        public void StackCountValid(int[] cases)
        {
            foreach(int num in cases)
            {
                stack.Push(num);
            }

            Assert.AreEqual(stack.Count, cases.Length);
        }

        [TestMethod]
        [DataTestMethod]
        [CsvTestSource("CCTestData.csv")]
        public void PushAndPeekTest(int[] cases)
        {
            foreach(int num in cases)
            {
                stack.Push(num);
                Assert.AreEqual(stack.Peek(), num);
            }
        }

        [TestMethod]
        [DataTestMethod]
        [CsvTestSource("CCTestData.csv")]
        public void ClearStackTest(int[] cases)
        {
            foreach (int num in cases)
            {
                stack.Push(num);
            }

            stack.Clear();
            Assert.AreEqual(stack.Count, 0);
        }

        [TestMethod]
        [DataTestMethod]
        [CsvTestSource("CCTestData.csv")]
        public void PushAndPopTest(int[] cases)
        {
            foreach (int num in cases)
            {
                stack.Push(num);
                Assert.AreEqual(stack.Pop(), num);
                Assert.AreEqual(stack.Count, 0);
            }
        }
    }
}
