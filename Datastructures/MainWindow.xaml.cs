﻿using Datastructures.CodeEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Datastructures
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ParenChecker parenChecker = new ParenChecker();
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Editor_textbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.Source == editor_textbox)
            {
                parenChecker.CheckText(editor_textbox, editor_statusbar_label);
            }
        }
    }
}
