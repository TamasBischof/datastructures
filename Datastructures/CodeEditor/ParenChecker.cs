﻿using System;
using System.Collections.Generic;
using Datastructures.CL;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Datastructures.CodeEditor
{
    /// <summary>
    /// Responsible for checking if each left-hand parenthesis is matched by a right-hand one.
    /// </summary>
    class ParenChecker
    {
        List<char> leftParenCharacters = new List<char> { '(', '[', '{', };
        List<char> rightParenCharacters = new List<char> { ')', ']', '}' };

        /// <summary>
        /// Perform a check whether all parentheses are balanced in a text.
        /// </summary>
        /// <param name="text">String to examine.</param>
        /// <param name="position">Output variable to store the character index where the first error was found.</param>
        internal void CheckText(TextBox textBox, TextBlock statusBar)
        {
            CL.Stack<KeyValuePair<int,char>> parenStack = new CL.Stack<KeyValuePair<int,char>>(); //stack to hold left-hand parentheses for matching
            int counter = 0;
            foreach (char c in textBox.Text)
            {
                if (leftParenCharacters.Contains(c))
                {
                    parenStack.Push(new KeyValuePair<int,char>(counter,c));
                }
                else if (rightParenCharacters.Contains(c))
                {
                    if (parenStack.Count > 0 && parenStack.Peek().Value == leftParenCharacters[rightParenCharacters.IndexOf(c)])
                    {
                        parenStack.Pop();
                    }
                    else
                    {
                        if (parenStack.Count == 0)
                        {
                            statusBar.Text = string.Format("The right-hand parenthesis character '{0}' at position {1} appears to be unmatched!", c, counter);
                            return;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                counter++;
            }
            statusBar.Text = parenStack.Count == 0 ? "" : string.Format("The left-hand parenthesis character '{0}' at position {1} appears to be unmatched!", parenStack.Peek().Value, parenStack.Peek().Key);
        }
    }
}
