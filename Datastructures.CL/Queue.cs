﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datastructures.CL
{
    /// <summary>
    /// Simple queue data structure, implemented with LinkedList.
    /// </summary>
    /// <typeparam name="T">Type of items to be stored in the queue.</typeparam>
    public class Queue<T> : IEnumerable<T>
    {
        LinkedList<T> list = new LinkedList<T>();

        public int Count { get { return list.Count; } }

        /// <summary>
        /// Puts an item at the end of the queue.
        /// </summary>
        /// <param name="item">Item to add.</param>
        public void Enqueue(T item)
        {
            list.Add(item);
        }

        /// <summary>
        /// Removes the item at the front of the queue.
        /// </summary>
        public T Dequeue()
        {
            if (Count == 0)
            {
                throw new CollectionEmptyException("Item could not be retrieved: queue empty");
            }
            T item = list.Head.Value;
            list.RemoveHead();
            return item;
        }

        /// <summary>
        /// Returns the item at the front of the queue without removing it.
        /// </summary>
        /// <returns>The item currently at the front.</returns>
        public T Peek()
        {
            if (Count == 0)
            {
                throw new CollectionEmptyException("Item could not be retrieved: queue empty");
            }
            return list.Head.Value;
        }

        /// <summary>
        /// Clears the queue of all its elements.
        /// </summary>
        public void Clear()
        {
            list.Clear();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return list.GetEnumerator();
        }
    }
}
