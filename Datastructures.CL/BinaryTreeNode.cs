﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datastructures.CL
{
    /// <summary>
    /// Represents a node on a binary tree structure. Stores a single value.
    /// </summary>
    /// <typeparam name="T">The type of the value contained.</typeparam>
    public class BinaryTreeNode<T> : IComparable<T> where T : IComparable
    {
        /// <summary>
        /// Value stored by this node.
        /// </summary>
        public T Value { get; set; }
        /// <summary>
        /// The node assigned as this node's left child.
        /// </summary>
        public BinaryTreeNode<T> LeftChild { get; set; }
        /// <summary>
        /// The node assigned as this node's right child.
        /// </summary>
        public BinaryTreeNode<T> RightChild { get; set; }

        public BinaryTreeNode(T value) => Value = value;

        public int CompareTo(T other)
        {
            return Value.CompareTo(other);
        }
    }
}
