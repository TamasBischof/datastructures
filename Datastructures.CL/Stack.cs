﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datastructures.CL
{
    public class Stack<T> : IEnumerable<T>
    {
        LinkedList<T> list = new LinkedList<T>();

        public int Count
        {
            get
            {
                return list.Count;
            }
        }

        public void Push(T item)
        {
            list.AddToHead(item);
        }

        public T Pop()
        {
            if (list.Empty)
            {
                throw new CollectionEmptyException("Could not retrieve element: stack is empty");
            }
            T item = list.Head.Value;
            list.RemoveHead();
            return item;
        }

        public T Peek()
        {
            if (list.Empty)
            {
                throw new CollectionEmptyException("Could not retrieve element: stack is empty");
            }
            return list.Head.Value;
        }

        public void Clear()
        {
            list.Clear();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return list.GetEnumerator();
        }

    }
}
