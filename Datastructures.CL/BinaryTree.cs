﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datastructures.CL
{
    /// <summary>
    /// Represents an unbalanced binary tree where values are sorted left to right (each node has smaller values on its left subtree, larger values on its right subtree).
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BinaryTree<T> : IEnumerable<T> where T : IComparable
    {
        BinaryTreeNode<T> rootNode;
        public int Count { get; private set; }
        public bool IsEmpty => Count == 0;

        /// <summary>
        /// Inserts a value into the tree.
        /// </summary>
        /// <param name="value">The value to add.</param>
        public void Insert(T value)
        {
            if (rootNode == null)
            {
                rootNode = new BinaryTreeNode<T>(value);
            }
            else
            {
                InsertAtNode(rootNode, value);
            }

            Count++;
        }

        /// <summary>
        /// Convenience method to insert a collection of values into the tree. Insertion is done from first to last.
        /// </summary>
        /// <param name="values">Values to insert.</param>
        public void InsertRange(IEnumerable<T> values)
        {
            foreach(var value in values)
            {
                Insert(value);
            }
        }

        //recursively inserts a node into the tree
        void InsertAtNode(BinaryTreeNode<T> node, T value)
        {
            if (value.CompareTo(node.Value) < 0)
            {
                if (node.LeftChild == null)
                {
                    node.LeftChild = new BinaryTreeNode<T>(value);
                }
                else
                {
                    InsertAtNode(node.LeftChild, value);
                }
            }
            else
            {
                if (node.RightChild == null)
                {
                    node.RightChild = new BinaryTreeNode<T>(value);
                }
                else
                {
                    InsertAtNode(node.RightChild, value);
                }
            }
        }

        /// <summary>
        /// Check whether a value is in the tree.
        /// </summary>
        /// <param name="value">The value to find.</param>
        public bool Contains(T value)
        {
            return FindWithParent(value, out _) != null;
        }

        public void Remove(T value)
        {
            BinaryTreeNode<T> parent;
            var nodeToRemove = FindWithParent(value, out parent);

            if (nodeToRemove == null) return;

            //if there's no right child, substitute with left subtree
            if (nodeToRemove.RightChild == null)
            {
                if (parent == null)
                {
                    rootNode = nodeToRemove.LeftChild;
                }
                else
                {
                    if (parent.CompareTo(nodeToRemove.Value) < 0)
                    {
                        parent.RightChild = nodeToRemove.LeftChild;
                    }
                    else
                    {
                        parent.LeftChild = nodeToRemove.LeftChild;
                    }
                }
            }

            //if there's no left subtree under the right subtree
            else if (nodeToRemove.RightChild.LeftChild == null)
            {
                //put the node's left subtree in place of the missing subtree
                nodeToRemove.RightChild.LeftChild = nodeToRemove.LeftChild;

                //shift the right subtree in place of the removed node
                if (parent == null)
                {
                    rootNode = nodeToRemove.RightChild;
                }
                else
                {
                    //check if the node to remove was on the right and assign the new subtree
                    if (parent.CompareTo(nodeToRemove.Value) < 0)
                    {
                        parent.RightChild = nodeToRemove.RightChild;
                    }
                    //or left
                    else
                    {
                        parent.LeftChild = nodeToRemove.RightChild;
                    }

                }
            }

            //if there is a left subtree under the right subtree
            else if (nodeToRemove.RightChild.LeftChild != null)
            {
                //find the smallest valued node in the whole subtree
                BinaryTreeNode<T> smallestNode = nodeToRemove.RightChild.LeftChild;
                BinaryTreeNode<T> parentOfSmallest = nodeToRemove.RightChild;

                while(smallestNode.LeftChild != null)
                {
                    parentOfSmallest = smallestNode;
                    smallestNode = smallestNode.LeftChild;
                }

                //1. in case the smallest had a right subtree, hand it over to the parent
                parentOfSmallest.LeftChild = smallestNode.RightChild;

                //2. assign the removed node's subtrees to the smallest node
                smallestNode.LeftChild = nodeToRemove.LeftChild; //should be null
                smallestNode.RightChild = nodeToRemove.RightChild;

                //3. then put the smallest node in place of the one being removed
                if (parent == null) //if the root is being removed
                {
                    rootNode = smallestNode;
                }

                //if not the root, check which subtree of the parent the removed node belonged to
                else
                {
                    if (parent.CompareTo(nodeToRemove.Value) < 0)
                    {
                        parent.RightChild = smallestNode;
                    }
                    else
                    {
                        parent.LeftChild = smallestNode;
                    } 
                }
            }

        }

        /// <summary>
        /// Finds a node that contains a value, and assigns its parent to an out parameter.
        /// </summary>
        /// <param name="value">Value to find.</param>
        /// <param name="parent">Out parameter for the node's parent.</param>
        /// <returns>The node containing the value, or null if the value is not in the tree.</returns>
        private BinaryTreeNode<T> FindWithParent(T value, out BinaryTreeNode<T> parent)
        {
            BinaryTreeNode<T> current = rootNode;
            parent = null;
            int comparisonResult;

            while (current != null)
            {
                comparisonResult = value.CompareTo(current.Value);
                if (comparisonResult < 0)
                {
                    parent = current;
                    current = current.LeftChild;
                }
                else if (comparisonResult > 0)
                {
                    parent = current;
                    current = current.RightChild;
                }
                else
                {
                    break;
                }

            }
            return current;

        }

        public IEnumerator<T> GetEnumerator()
        {
            if (rootNode == null)
            {
                yield return default(T);
            }
            foreach (T value in InOrderTraversal(rootNode))
            {
                yield return value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerable<T> InOrderTraversal(BinaryTreeNode<T> node)
        {
            if (node.LeftChild != null)
            {
                foreach (T value in InOrderTraversal(node.LeftChild))
                {
                    yield return value;
                } 
            }
            yield return node.Value;
            if (node.RightChild != null)
            {
                foreach(T value in InOrderTraversal(node.RightChild))
                {
                    yield return value;
                }
            }
        }
    }
}
