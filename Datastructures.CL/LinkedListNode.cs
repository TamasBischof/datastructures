﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datastructures.CL
{
    /// <summary>
    /// Node for a linked list.
    /// </summary>
    /// <typeparam name="T">Type of the value this node holds.</typeparam>
    public class LinkedListNode<T>
    {

        public LinkedListNode(T item)
        {
            Value = item;
        }

        /// <summary>
        /// Reference to the previous element of the linked list.
        /// </summary>
        public LinkedListNode<T> Previous { get; set; }
        /// <summary>
        /// Reference to the next element of the linked list.
        /// </summary>
        public LinkedListNode<T> Next { get; set; }
        /// <summary>
        /// The value the node holds.
        /// </summary>
        public T Value { get; private set; }
    }
}
