﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datastructures.CL
{
    public class CollectionEmptyException : Exception
    {
        public CollectionEmptyException()
        {

        }

        public CollectionEmptyException(string message) : base(message)
        {

        }

        public CollectionEmptyException(string message, Exception inner) : base(message,inner)
        {

        }
    }
}
