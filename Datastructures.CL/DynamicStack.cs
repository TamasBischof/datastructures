﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datastructures.CL
{
    public class DynamicStack<T> : IEnumerable<T>
    {
        DynamicArray<T> array = new DynamicArray<T>();

        public int Count => array.Count;

        public void Push(T item)
        {
            array.Add(item);
        }

        public T Peek()
        {
            if (array.Count == 0)
            {
                throw new CollectionEmptyException("Could not retrieve element: stack is empty");
            }
            return array[array.Count - 1];
        }

        public T Pop()
        {
            if (array.Count == 0)
            {
                throw new CollectionEmptyException("Could not retrieve element: stack is empty"); 
            }
            T item = Peek();
            array.RemoveWithIndex(array.Count - 1);
            return item;
        }

        public void Clear()
        {
            array.Clear();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return array.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return array.GetEnumerator();
        }
    }
}
