﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datastructures.CL
{
    public class DynamicArray<T> : IEnumerable<T>, ICollection<T>
    {
        public DynamicArray()
        {
            array = new T[2];
        }

        T[] array;

        int Capacity => array.Length;

        public int Count { get; private set; }

        public bool IsReadOnly => false;

        /// <summary>
        /// Adds an item to the end of this collection.
        /// </summary>
        /// <param name="item">Item to add.</param>
        public void Add(T item)
        {
            //grow as needed
            if (Count == Capacity)
            {
                T[] newArray = new T[array.Length * 2];
                array.CopyTo(newArray, 0);
                array = newArray;
            }

            array[Count] = item;
            Count++;
        }

        /// <summary>
        /// Adds a collection of items to this collection.
        /// </summary>
        /// <param name="items">Items to add.</param>
        public void AddRange(IEnumerable<T> items)
        {
            foreach(var item in items)
            {
                Add(item);
            }
        }

        public T this[int index]
        {
            get
            {
                if (index >= Count)
                {
                    throw new IndexOutOfRangeException();
                }
                return array[index];
            }
            set
            {
                if (index >= Count)
                {
                    throw new IndexOutOfRangeException();
                }
                array[index] = value;
            }
        } 

        /// <summary>
        /// Removes all elements from this collection.
        /// </summary>
        public void Clear()
        {
            Count = 0;
        }

        /// <summary>
        /// Returns whether an item is part of the collection.
        /// </summary>
        /// <param name="item">Item to search for.</param>
        /// <returns>Whether the collection contains the item.</returns>
        public bool Contains(T item)
        {
            foreach(var element in array)
            {
                if (element.Equals(item))
                {
                    return true;
                }
            }
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            array.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)array).GetEnumerator();
        }

        /// <summary>
        /// Removes the specified item from this collection.
        /// </summary>
        /// <param name="item">Item to remove.</param>
        /// <returns>True if the item was found and removed.</returns>
        public bool Remove(T item)
        {
            int indexToRemove = 0;
            bool itemFound = false;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i].Equals(item))
                {
                    indexToRemove = i;
                    itemFound = true;
                    break;
                }
            }
            if (itemFound)
            {
                for (int i = indexToRemove; i < array.Length - 1; i++)
                {
                    array[i] = array[i + 1];
                }
                Count--;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Removes an item at the specified index.
        /// </summary>
        /// <param name="index">Index to remove.</param>
        public void RemoveWithIndex(int index)
        {
            if (index >= Count)
            {
                throw new IndexOutOfRangeException();
            }
            for (int i = index; i < array.Length - 1; i++)
            {
                array[i] = array[i + 1];
            }
            Count--;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return array.GetEnumerator();
        }
    }
}
