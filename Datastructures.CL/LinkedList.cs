﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datastructures.CL
{
    /// <summary>
    /// Doubly-linked list.
    /// </summary>
    /// <typeparam name="T">Type of the value this list holds.</typeparam>
    public class LinkedList<T> : IEnumerable<T>, ICollection<T>
    {
        public LinkedListNode<T> Head { get; private set; }
        public LinkedListNode<T> Tail { get; private set; }
        
        public int Count { get; private set; }
        public bool Empty => Count == 0;
        public bool IsReadOnly => false;

        /// <summary>
        /// Adds an item to the end of the list.
        /// </summary>
        /// <param name="item">Item to add.</param>
        public void Add(T item)
        {
            AddToTail(item);
        }

        /// <summary>
        /// Adds a collection of items, in-order, to the end of the list.
        /// </summary>
        /// <param name="item">Collection of items to add.</param>
        public void AddRange(IEnumerable<T> items)
        {
            foreach(var item in items)
            {
                AddToTail(item);
            }
        }

        /// <summary>
        /// Adds an item to the end of the list.
        /// </summary>
        /// <param name="item">Item to add.</param>
        public void AddToTail(T item)
        {
            var node = new LinkedListNode<T>(item);
            //when adding to an empty list, set up the head and tail references properly
            if (Empty)
            {
                Head = node;
                Tail = node;
            }
            else
            {
                //push references back
                Tail.Next = node;
                node.Previous = Tail;
                Tail = node;
            }
            Count++;
        }

        /// <summary>
        /// Adds an item to the start of the list.
        /// </summary>
        /// <param name="item">Item to add.</param>
        public void AddToHead(T item)
        {
            //see AddToTail
            var node = new LinkedListNode<T>(item);
            if (Empty)
            {
                Head = node;
                Tail = node;
            }
            else
            {
                Head.Previous = node;
                node.Next = Head;
                Head = node;
            }
            Count++;
        }

        /// <summary>
        /// Clears out this collection.
        /// </summary>
        public void Clear()
        {
            //dereference head/tail and set count to 0, let GC do the rest.
            Head = null;
            Tail = null;
            Count = 0;
        }

        public bool Contains(T item)
        {
            return Find(item) != null;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            LinkedListNode<T> current = Head;
            while (current != null)
            {
                array[arrayIndex] = current.Value;
                current = current.Next;
                arrayIndex++;
            }
        }

        public bool Remove(T item)
        {
            if (!Empty)
            {
                LinkedListNode<T> toRemove = Find(item);
                if (toRemove != null)
                {
                    //if first element of the list, update the list accordingly
                    if (toRemove.Previous == null)
                    {
                        RemoveHead();
                        return true;
                    }
                    //same for tail elements
                    else if (toRemove.Next == null)
                    {
                        RemoveTail();
                        return true;
                    }
                    //otherwise update references around it
                    else
                    {
                        toRemove.Previous.Next = toRemove.Next;
                        toRemove.Next.Previous = toRemove.Previous;
                        Count--;
                        return true;
                    }
                }
            }
            return false;
        }

        public void RemoveHead()
        {
            if (!Empty)
            {
                Head = Head.Next;
                Count--;
                if (Empty)
                {
                    Tail = null;
                }
                else
                {
                    Head.Previous = null;
                }
            }
        }

        public void RemoveTail()
        {
            if (!Empty)
            {
                Tail = Tail.Previous;
                Count--;
                if (Empty)
                {
                    Head = null;
                }
                else
                {
                    Tail.Next = null;
                }
            }
        }

        /// <summary>
        /// Returns a reference to the first node containing a value.
        /// </summary>
        /// <param name="item">Item to find.</param>
        /// <returns>First node that contains the value.</returns>
        LinkedListNode<T> Find(T item)
        {
            LinkedListNode<T> current = Head;
            while (current != null)
            {
                if (current.Value.Equals(item))
                {
                    return current;
                }
                current = current.Next;
            }
            return null;
        }

        public IEnumerator<T> GetEnumerator()
        {
            LinkedListNode<T> current = Head;
            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>)this).GetEnumerator();
        }
    }
}
